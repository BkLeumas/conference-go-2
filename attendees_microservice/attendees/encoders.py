class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'name',
    ]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'email',
        'name',
        'company_name',
        'created',

    ]
