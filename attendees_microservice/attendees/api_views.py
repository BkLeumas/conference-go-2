from django.http import JsonResponse
# from encoders.py import ModelEncoder
from .models import Attendee, ConferenceVO
# from events.models import Conference
import json
# from events.views import ConferenceListEncoder
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'name',
    ]
class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'email',
        'name',
        'company_name',
        'created',
    ]


@require_http_methods(["POST", "GET"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            {'attendees': attendee},
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else: #GET
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {'attendees': attendees},
            encoder=AttendeeListEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {'attendee': attendee},
            encoder=AttendeeDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(conferences=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "conference does not exist"}
            )
        Attendee.objects.filter(id=id).update(**content)

        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse(
    #     {'attendee': attendee},
    #     encoder=AttendeeDetailEncoder,
    #     safe=False
    # )
    # elif request.method == "PUT":
    #     content = json.loads(request.body)
    #     try:
    #         location = Location.objects.get(name=content["location"])
    #         content["location"] = location
    #     except Location.DoesNotExist:
    #         return JsonResponse(
    #             {'message': 'invalid location'}
    #         )

    #     Conference.objects.filter(id=id).update(**content)

    #     conference = Conference.objects.get(id=id)
    #     return JsonResponse(
    #         conference,
    #         encoder=ConferenceDetailEncoder,
    #         safe=False,
    #     )
    # else:
    #     count, _ = Conference.objects.filter(id=id).delete()
    #     return JsonResponse({"deleted": count > 0})
